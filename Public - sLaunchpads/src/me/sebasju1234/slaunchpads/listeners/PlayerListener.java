package me.sebasju1234.slaunchpads.listeners;

import me.sebasju1234.slaunchpads.Launchpads;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class PlayerListener implements Listener {

	private Launchpads plugin;
	
	public PlayerListener(Launchpads plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onBlockPlacement(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		
		if (player.hasPermission("slaunchpads.create")) {
			if (event.getBlockPlaced().getType().getId() == plugin.config.getConfig().getInt("blockID")) {
				player.sendMessage(ChatColor.GOLD + "[" + ChatColor.YELLOW + "sLaunchpads" + ChatColor.GOLD + "] " + ChatColor.YELLOW + "You are now building a launchpad! Place a stone pressure plate on top of the block you've placed.");
			}
			
			if (event.getBlockPlaced().getType().equals(Material.STONE_PLATE)) {
				Location loc = event.getBlockPlaced().getLocation().clone();
				
				if (loc.add(0, -1, 0).getBlock().getType().getId() == plugin.config.getConfig().getInt("blockID")) {
					player.sendMessage(ChatColor.GOLD + "[" + ChatColor.YELLOW + "sLaunchpads" + ChatColor.GOLD + "] " + ChatColor.YELLOW + "You have made a launchpad!");
				}
			}
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Location loc = player.getLocation();
		Block block = event.getPlayer().getLocation().getBlock();
		
		if (event.getAction().equals(Action.PHYSICAL) && event.getClickedBlock().getType().equals(Material.STONE_PLATE)) {
			if (player.hasPermission("slaunchpads.use")) {
				player.playSound(player.getLocation(), Sound.BAT_TAKEOFF, 1, 1);
				
				if (event.getClickedBlock().getLocation().clone().add(0, -1, 0).getBlock().getType().getId() == plugin.config.getConfig().getInt("blockID")) {
					double rotX = loc.getYaw(), rotY = loc.getPitch(), cosY = Math.cos(Math.toRadians(rotY));
			        double x = (-cosY * Math.sin(Math.toRadians(rotX))), z = (cosY * Math.cos(Math.toRadians(rotX)));
					player.setVelocity(new Vector(x, cosY/8, z).multiply(plugin.config.getConfig().getInt("launchPower")));	
					
					event.getClickedBlock().getWorld().createExplosion(event.getClickedBlock().getLocation(), 0F);
				}
			}
		}
	}
	
}
