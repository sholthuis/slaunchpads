package me.sebasju1234.slaunchpads.data;

import java.io.File;

import me.sebasju1234.slaunchpads.Launchpads;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Configuration {

	private static Launchpads plugin;
	
	private static File dataFolder;
    private static File configFile;
    
    private static FileConfiguration config;
	
	public Configuration(Launchpads plugin) {
		this.plugin = plugin;
		
		this.dataFolder = plugin.getDataFolder();
        this.configFile = new File(plugin.getDataFolder(), "config.yml");
        
        this.config = YamlConfiguration.loadConfiguration(configFile);
	}
	
	public static void createFile() {
		if (!dataFolder.exists()) {
			dataFolder.mkdirs();
		}
		
		if (!configFile.exists()) {
            plugin.saveResource("config.yml", true);
		}
	}
	
	public static File getDataFolder() {
		return dataFolder;
	}
	
	public static File getConfigFile() {
		return configFile;
	}
	
	public static FileConfiguration getConfig() {
		return config;
	}
	
	public static void saveConfigFile(){
		try {
			config.save(getConfigFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
