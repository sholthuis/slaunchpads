package me.sebasju1234.slaunchpads;

import me.sebasju1234.slaunchpads.data.Configuration;
import me.sebasju1234.slaunchpads.listeners.PlayerListener;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;

public class Launchpads extends JavaPlugin {

	public Configuration config;
	
	public void log(String msg) {
		getServer().getLogger().info("[sLaunchpads] " + msg);
	}
	
	public void onEnable() {
		this.config = new Configuration(this);
		config.createFile();
		
		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		
		getServer().getPluginManager().addPermission(new Permission("slaunchpads.create", PermissionDefault.OP));
		getServer().getPluginManager().addPermission(new Permission("slaunchpads.use", PermissionDefault.OP));
		
		super.onEnable();
	}
	
	public void onDisable() {
		super.onDisable();
	}
	
}
